# YouCan Challenge

C'est un projet d'application mobile réalisé avec Flutter en utilisant l'architecture Model-View-ViewModel (MVVM). Ce projet utilise le package Provider pour la gestion de l'état, qui est recommandé par Flutter officiel.

## Structure du projet

Le projet est organisé comme suit :

- `main.dart` : Le point d'entrée de l'application.
- `core/` : Contient les éléments principaux du modèle, de la vue et du ViewModel.
  - `model/` : Dossier pour les fichiers de modèle.
  - `ViewModel/` : Dossier pour les fichiers ViewModel.
  - `services/` : Dossier pour les fichiers de service.
- `utils/` : Dossier pour les fichiers d'utilitaires.
- `views/` : Dossier pour les fichiers de vue.

## Prérequis

- [Flutter](https://flutter.dev/docs/get-started/install) doit être installé sur votre machine.
- Un éditeur de texte, par exemple [Visual Studio Code](https://code.visualstudio.com/) ou [Android Studio](https://developer.android.com/studio).

## Clonage du projet

Pour cloner ce projet sur votre machine locale, ouvrez un terminal et exécutez la commande suivante :
git clone https://gitlab.com/Abderrahmane_Dinoune/youcan-challenge


Remplacez `<votre nom d'utilisateur>` et `<nom du projet>` par votre nom d'utilisateur GitLab et le nom du projet respectivement.

## Exécution du projet

Pour exécuter ce projet sur votre machine locale, suivez les étapes ci-dessous :

1. Naviguez vers le dossier du projet dans le terminal.
2. Exécutez `flutter pub get` pour télécharger toutes les dépendances nécessaires.
3. Lancez un émulateur Android ou iOS, ou connectez un appareil physique.
4. Exécutez `flutter run` dans le terminal pour lancer l'application.

## Contribuer

Pour contribuer à ce projet, veuillez créer une nouvelle branche à partir de la branche `main`, apportez vos modifications et soumettez une merge request.

## Licence

Ce projet est sous licence [MIT](LICENSE).
