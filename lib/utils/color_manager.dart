import 'package:flutter/material.dart';

class ColorManager {
  static const Color primary = Color(0xff6f2f8f);
  static const Color darkGrey = Color(0xff525252);
  static const Color grey = Color(0xff737477);
  static const Color lightGrey = Color(0xff9E9E9E);
  static const Color black = Color(0xff000000);

  static const Color secondaryColor = Color(0xFFa2388d);
  static const Color darkBlue = Color(0xff233648);
  static const Color darkOrange = Color(0xffFEB23C);
  static const Color blue = Color(0xff49AFDA);
  static const Color orange = Color(0xffFB9307);
  static const Color blueTiede = Color(0xFF21B7CA);

  static const Color blueAvatarBackground = Color(0xFFA5D7E8);

  // new colors
  static const Color darkPrimary = Color(0xffd17d11);
  static const Color lightPrimary = Color(0xCCd17d11); // color with 80% opacity
  static const Color grey1 = Color(0xff707070);
  static const Color grey2 = Color(0xff797979);
  static const Color white = Color(0xffFFFFFF);
  static const Color error = Color(0xffe61f34); // red color

  static const Color enCours = Color(0xff205081);
  static const Color envoyer = Color(0xff205081);
  static const Color reviser = Color(0xffFF9E42);
  static const Color signe = Color(0xff205081);
  static const Color refuser = Color(0xffEA5455);
  static const Color expirer = Color(0xff205081);
}
