import 'package:flutter/material.dart';
import 'package:youcan_chalenge/utils/color_manager.dart';
import 'package:youcan_chalenge/utils/font_manager.dart';
import 'package:youcan_chalenge/utils/styles_manager.dart';
import 'package:youcan_chalenge/utils/values_manager.dart';

ThemeData getApplicationTheme() {
  return ThemeData(
      useMaterial3: true,
      // main colors

      primaryColor: ColorManager.primary,
      primaryColorLight: ColorManager.lightPrimary,
      primaryColorDark: ColorManager.darkPrimary,
      disabledColor: ColorManager.grey1,
      splashColor: ColorManager.lightPrimary,
      // ripple effect color
      dialogBackgroundColor: ColorManager.white,
      // cardview theme
      cardTheme: const CardTheme(
          color: ColorManager.white,
          shadowColor: ColorManager.grey,
          elevation: AppSize.s4),
      // app bar theme
      appBarTheme: const AppBarTheme(
        centerTitle: true,
        color: ColorManager.darkBlue,
        foregroundColor: ColorManager.white,
      ),
      // button theme
      buttonTheme: const ButtonThemeData(
          shape: StadiumBorder(),
          disabledColor: ColorManager.grey1,
          buttonColor: ColorManager.primary,
          splashColor: ColorManager.lightPrimary),

      // elevated button them
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          textStyle: getRegularStyle(
            color: ColorManager.white,
            fontSize: FontSize.s17,
          ),
          backgroundColor: ColorManager.primary,
          foregroundColor: ColorManager.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(AppSize.s12),
          ),
        ),
      ),
      textTheme: TextTheme(
        displayLarge: getSemiBoldStyle(
          color: ColorManager.darkBlue,
        ),
        headlineLarge: getSemiBoldStyle(
          color: ColorManager.darkBlue,
        ),
        headlineMedium: getRegularStyle(
          color: ColorManager.darkBlue,
        ),
        titleMedium: getMediumStyle(
          color: ColorManager.darkBlue,
        ),
        titleSmall: getRegularStyle(
          color: ColorManager.darkBlue,
        ),
        bodyLarge: getRegularStyle(color: ColorManager.darkBlue),
        bodySmall: getRegularStyle(color: ColorManager.darkBlue),
        bodyMedium: getRegularStyle(
          color: ColorManager.darkBlue,
        ),
        labelSmall: getBoldStyle(
          color: ColorManager.darkBlue,
        ),
      ),

      // input decoration theme (text form field)
      inputDecorationTheme: InputDecorationTheme(
        // content padding
        contentPadding: const EdgeInsets.all(AppPadding.p8),
        // hint style
        hintStyle:
            getRegularStyle(color: ColorManager.grey, fontSize: FontSize.s14),
        labelStyle:
            getMediumStyle(color: ColorManager.grey, fontSize: FontSize.s14),
        errorStyle: getRegularStyle(color: ColorManager.error),

        // enabled border style
        enabledBorder: const OutlineInputBorder(
            borderSide:
                BorderSide(color: ColorManager.grey, width: AppSize.s1_5),
            borderRadius: BorderRadius.all(Radius.circular(20))),

        // focused border style
        focusedBorder: const OutlineInputBorder(
            borderSide:
                BorderSide(color: ColorManager.primary, width: AppSize.s1_5),
            borderRadius: BorderRadius.all(Radius.circular(20))),

        // error border style
        errorBorder: const OutlineInputBorder(
            borderSide:
                BorderSide(color: ColorManager.error, width: AppSize.s1_5),
            borderRadius: BorderRadius.all(Radius.circular(AppSize.s8))),
        // focused border style
        focusedErrorBorder: const OutlineInputBorder(
          borderSide:
              BorderSide(color: ColorManager.primary, width: AppSize.s1_5),
          borderRadius: BorderRadius.all(
            Radius.circular(AppSize.s8),
          ),
        ),
        suffixIconColor: ColorManager.darkBlue,
        prefixIconColor: ColorManager.darkBlue,
      ),
      floatingActionButtonTheme: const FloatingActionButtonThemeData(
        foregroundColor: Colors.white,
        backgroundColor: Colors.black,
      ));
}
