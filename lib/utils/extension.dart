extension DateTimeFormatting on DateTime {
  String toArabicFormat() {
    final List<String> months = [
      'يناير',
      'فبراير',
      'مارس',
      'أبريل',
      'مايو',
      'يونيو',
      'يوليو',
      'أغسطس',
      'سبتمبر',
      'أكتوبر',
      'نوفمبر',
      'ديسمبر',
    ];

    final String month = months[this.month - 1];
    final int day = this.day;
    final int year = this.year;

    return '$year $month $day';
  }
}
