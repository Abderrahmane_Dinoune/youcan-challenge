import 'package:flutter/material.dart';
import 'package:youcan_chalenge/views/home/home_screen.dart';

class Routes {
  static const String homeRoute = "/";
}

class RouteGenerator {
  static Route<dynamic> getRoute(RouteSettings settings) {
    switch (settings.name) {
      case Routes.homeRoute:
        return MaterialPageRoute(builder: (_) => const HomeScreen());
      default:
        return unDefinedRoute();
    }
  }

  static Route<dynamic> unDefinedRoute() {
    return MaterialPageRoute(
        builder: (_) => Scaffold(
              appBar: AppBar(
                title: const Text("Error"),
              ),
              body: const Center(child: Text("Navigation Error")),
            ));
  }
}
