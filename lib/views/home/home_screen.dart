import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:provider/provider.dart';
import 'package:youcan_chalenge/core/view_model/home_view_model.dart';
import 'package:youcan_chalenge/utils/assets_manager.dart';
import 'package:youcan_chalenge/utils/color_manager.dart';
import 'package:youcan_chalenge/utils/extension.dart';
import 'package:youcan_chalenge/views/home/widget/drawer_widget.dart';
import 'package:youcan_chalenge/views/home/widget/post_item_list.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<HomeViewModel>(context, listen: false).getPostList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //AppBar
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [
                ColorManager.primary,
                ColorManager.secondaryColor,
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
          ),
        ),
        title: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                AssetsManager.nextMediaLogo,
                height: 56, // Hauteur de l'image
                fit: BoxFit.cover, // Redimensionner l'image
              ),
              const Text(
                "Next\nMedia",
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
              )
            ],
          ),
        ),
      ),

      //Drawer
      endDrawer: const CustomDrawer(),

      //Body
      body: Consumer<HomeViewModel>(
        builder: (context, vm, widget) {
          return Container(
            margin: const EdgeInsets.symmetric(horizontal: 15),
            child: Visibility(
              visible: !vm.loading,
              replacement: const Center(child: CircularProgressIndicator()),
              child: vm.postsResponse == null
                  ? const Center(
                      child: Icon(
                        Icons.error,
                        size: 40,
                        color: ColorManager.primary,
                      ),
                    )
                  : Scrollbar(
                      child: ListView.separated(
                        controller: vm.listViewcontroller,
                        itemBuilder: (context, index) {
                          if (index < vm.postsResponse!.length) {
                            return PostItemList(
                                postsResponse: vm.postsResponse![index]);
                          } else {
                            return const Center(
                                child: CircularProgressIndicator());
                          }
                        },
                        itemCount: vm.postsResponse!.length + 1,
                        separatorBuilder: (context, index) {
                          return const Divider();
                        },
                      ),
                    ),
            ),
          );
        },
      ),
    );
  }
}
