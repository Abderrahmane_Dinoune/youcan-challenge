import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:youcan_chalenge/core/model/postes_response.dart';
import 'package:youcan_chalenge/utils/extension.dart';

class PostItemList extends StatelessWidget {
  PostItemList({super.key, required this.postsResponse});
  PostsResponse postsResponse;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Container(
        margin: const EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Image.network(postsResponse.yoastHeadJson!.ogImage![0].url!),
            Html(
              data: postsResponse.title!.rendered,
              style: {
                "body": Style(
                  textAlign: TextAlign.justify,
                  direction: TextDirection.rtl,
                  fontSize: FontSize.large,
                ),
              },
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 10),
              child: Text(
                postsResponse.yoastHeadJson!.articlePublishedTime!
                    .toArabicFormat(),
                style: const TextStyle(color: Colors.grey, fontSize: 10),
              ),
            )
          ],
        ),
      ),
    );
  }
}
