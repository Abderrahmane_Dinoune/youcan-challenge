import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:youcan_chalenge/core/view_model/home_view_model.dart';
import 'package:youcan_chalenge/utils/routes_manager.dart';
import 'package:youcan_chalenge/utils/theme_manager.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => HomeViewModel()),
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: getApplicationTheme(),
      debugShowCheckedModeBanner: false,
      initialRoute: Routes.homeRoute,
      onGenerateRoute: RouteGenerator.getRoute,
    );
  }
}
