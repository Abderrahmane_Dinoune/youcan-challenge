import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import '../../model/postes_response.dart';

class AppPreferences {
  static Future<void> setPostsList(String data) async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    sharedPreferences.setString("POSTS_LIST", data);
  }

  static Future<dynamic> getPostsList() async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    return List<PostsResponse>.from(
        jsonDecode(sharedPreferences.getString("POSTS_LIST") ?? "[]")
            .map((x) => PostsResponse.fromJson(x)));
  }

  static Future<void> setPageNumber(int pageNumber) async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    sharedPreferences.setInt("PAGE_NUMBER", pageNumber);
  }

  static Future<int?> getPageNamber() async {
    final SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
    return sharedPreferences.getInt("PAGE_NUMBER");
  }
}
