import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:youcan_chalenge/core/model/postes_response.dart';

import '../../../utils/constants_manager.dart';

class PostApi {
  static Future<dynamic> getPostListApi(int pageNumber) async {
    try {
      http.Response response = await http
          .get(
        Uri.parse(
          AppConstants.baseUrl + pageNumber.toString(),
        ),
      )
          .timeout(
        const Duration(seconds: 500),
        onTimeout: () {
          throw TimeoutException('The connection has timed out.');
        },
      );
      print(response.body);
      return List<PostsResponse>.from(
          jsonDecode(response.body).map((x) => PostsResponse.fromJson(x)));
    } catch (e) {
      print(e.toString());
    }
  }
}
