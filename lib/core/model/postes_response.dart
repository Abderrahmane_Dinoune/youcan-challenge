class PostsResponse {
  int? id;
  DateTime? date;
  DateTime? dateGmt;
  Guid? guid;
  DateTime? modified;
  DateTime? modifiedGmt;
  Guid? title;
  Content? content;
  YoastHeadJson? yoastHeadJson;

  PostsResponse({
    this.id,
    this.date,
    this.dateGmt,
    this.guid,
    this.modified,
    this.modifiedGmt,
    this.title,
    this.content,
    this.yoastHeadJson,
  });

  factory PostsResponse.fromJson(Map<String, dynamic> json) => PostsResponse(
        id: json["id"],
        date: json["date"] == null ? null : DateTime.parse(json["date"]),
        dateGmt:
            json["date_gmt"] == null ? null : DateTime.parse(json["date_gmt"]),
        guid: json["guid"] == null ? null : Guid.fromJson(json["guid"]),
        modified:
            json["modified"] == null ? null : DateTime.parse(json["modified"]),
        modifiedGmt: json["modified_gmt"] == null
            ? null
            : DateTime.parse(json["modified_gmt"]),
        title: json["title"] == null ? null : Guid.fromJson(json["title"]),
        content:
            json["content"] == null ? null : Content.fromJson(json["content"]),
        yoastHeadJson: json["yoast_head_json"] == null
            ? null
            : YoastHeadJson.fromJson(json["yoast_head_json"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "date": date?.toIso8601String(),
        "date_gmt": dateGmt?.toIso8601String(),
        "guid": guid?.toJson(),
        "modified": modified?.toIso8601String(),
        "modified_gmt": modifiedGmt?.toIso8601String(),
        "title": title?.toJson(),
        "content": content?.toJson(),
        "yoast_head_json": yoastHeadJson?.toJson(),
      };
}

class Content {
  String? rendered;
  bool? protected;

  Content({
    this.rendered,
    this.protected,
  });

  factory Content.fromJson(Map<String, dynamic> json) => Content(
        rendered: json["rendered"],
        protected: json["protected"],
      );

  Map<String, dynamic> toJson() => {
        "rendered": rendered,
        "protected": protected,
      };
}

class Guid {
  String? rendered;

  Guid({
    this.rendered,
  });

  factory Guid.fromJson(Map<String, dynamic> json) => Guid(
        rendered: json["rendered"],
      );

  Map<String, dynamic> toJson() => {
        "rendered": rendered,
      };
}

class YoastHeadJson {
  DateTime? articlePublishedTime;
  List<OgImage>? ogImage;

  YoastHeadJson({
    this.articlePublishedTime,
    this.ogImage,
  });

  factory YoastHeadJson.fromJson(Map<String, dynamic> json) => YoastHeadJson(
        articlePublishedTime: json["article_published_time"] == null
            ? null
            : DateTime.parse(json["article_published_time"]),
        ogImage: json["og_image"] == null
            ? []
            : List<OgImage>.from(
                json["og_image"]!.map((x) => OgImage.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "article_published_time": articlePublishedTime?.toIso8601String(),
        "og_image": ogImage == null
            ? []
            : List<dynamic>.from(ogImage!.map((x) => x.toJson())),
      };
}

class OgImage {
  int? width;
  int? height;
  String? url;
  String? type;

  OgImage({
    this.width,
    this.height,
    this.url,
    this.type,
  });

  factory OgImage.fromJson(Map<String, dynamic> json) => OgImage(
        width: json["width"],
        height: json["height"],
        url: json["url"],
        type: json["type"],
      );

  Map<String, dynamic> toJson() => {
        "width": width,
        "height": height,
        "url": url,
        "type": type,
      };
}
