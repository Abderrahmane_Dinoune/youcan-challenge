import 'package:flutter/material.dart';
import 'package:youcan_chalenge/core/model/postes_response.dart';
import 'package:youcan_chalenge/core/services/remote/post_api.dart';

class HomeViewModel extends ChangeNotifier {
  HomeViewModel() {
    listViewcontroller.addListener(() async {
      if (listViewcontroller.position.maxScrollExtent ==
          listViewcontroller.offset) {
        _loadingPagination = true;
        notifyListeners();

        final response = await PostApi.getPostListApi(pageNumber);

        _loadingPagination = false;
        notifyListeners();

        if (response != null) {
          _postsResponse = List<PostsResponse>.from(_postsResponse!)
            ..addAll(response);

          _pageNumber++;

          notifyListeners();
        }
      }
    });
  }

  List<PostsResponse>? _postsResponse;
  List<PostsResponse>? get postsResponse => _postsResponse;

  int _pageNumber = 1;
  int get pageNumber => _pageNumber;

  bool _loading = false;
  bool get loading => _loading;

  bool _loadingPagination = false;
  bool get loadingPagination => _loadingPagination;

  final ScrollController _listViewcontroller = ScrollController();
  ScrollController get listViewcontroller => _listViewcontroller;

  Future<void> getPostList() async {
    _loading = true;
    notifyListeners();

    final response = await PostApi.getPostListApi(pageNumber);

    if (response != null) {
      _postsResponse = response;
    }

    _loading = false;
    notifyListeners();
  }
}
